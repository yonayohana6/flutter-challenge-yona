import 'package:auth_products/app/app.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthInitial()) {
    on<CheckAuth>((event, emit) async {
      await Helpers.getToken().then((token) {
        if (token == '') {
          isAuth = false;
        }
      });
      if (isAuth == false) {
        emit(AuthFailed());
      } else {
        emit(AuthSuccess());
      }
    });
  }

  bool isAuth = true;
}
