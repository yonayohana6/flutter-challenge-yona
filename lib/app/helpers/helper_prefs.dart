import 'package:shared_preferences/shared_preferences.dart';

class Helpers {
  //set token login
  static Future setToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString('token', token);
  }

  //get token
  static Future<String> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('token') ?? '';
  }

  //set id user
  static Future setID(int id) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setInt('id', id);
  }

  //get id user
  static Future<int> getID() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt('id') ?? 0;
  }

  static Future logout() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    prefs.remove('id');
  }
}
