import 'dart:convert';

import 'package:auth_products/app/app.dart';
import 'package:auth_products/home/home.dart';
import 'package:http/http.dart' as http;

class ApiServices {
  final url = Constan.baseUrl;

  Future login(String username, String password) async {
    final response = await http.post(
      Uri.parse("${url}auth/login"),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'username': username,
        'password': password,
      }),
    );
    final statusCode = response.statusCode;
    final output = jsonDecode(response.body);
    final token = output['token'];
    if (statusCode == 200) {
      final id = output["id"];
      // print(output);
      // print(token);
      Helpers.setToken(token);
      Helpers.setID(id);
    }
    return statusCode;
  }

  Future<List<Product>> getProduct() async {
    final response = await http.get(
      Uri.parse(
        "${url}products",
      ),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    final output = jsonDecode(response.body);
    final product = output['products'] as List;
    if (response.statusCode == 200) {
      // final dataProduct =
      // print(dataProduct);
      return product
          .map<Product>((e) => Product.fromJson(e as Map<String, dynamic>))
          .toList();
    } else {
      throw Exception('Failed to load');
    }
  }
}
