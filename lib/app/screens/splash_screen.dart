import 'dart:async';

import 'package:auth_products/app/app.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  _setTimeSplash() {
    Timer(const Duration(seconds: 2), () {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const AppScreen(),
        ),
      );
    });
  }

  @override
  void initState() {
    _setTimeSplash();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Image.asset(
            "assets/images/header-splash.png",
          ),
          Flexible(flex: 1, child: Container()),
          Image.asset(
            "assets/images/logo.png",
            scale: 1.5,
          ),
          Flexible(flex: 1, child: Container()),
          Image.asset(
            "assets/images/footer-splash.png",
          ),
        ],
      ),
    );
  }
}
