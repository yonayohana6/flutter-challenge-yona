import 'package:auth_products/login/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 350,
      margin: const EdgeInsets.fromLTRB(25, 0, 25, 0),
      child: Form(
        key: context.read<LoginBloc>().formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  'Login',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 10),
                Text('Please sign in to continue')
              ],
            ),
            const SizedBox(height: 40),
            const Text(
              'User ID',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            TextFormField(
              controller: context.read<LoginBloc>().username,
              decoration: InputDecoration(
                enabledBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey[800]!,
                  ),
                ),
                hintText: 'User ID',
                hintStyle: const TextStyle(
                  fontSize: 12,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
            const SizedBox(height: 30),
            const Text(
              'Password',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            BlocBuilder<LoginBloc, LoginState>(
              builder: (context, state) {
                return TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: context.read<LoginBloc>().password,
                  decoration: InputDecoration(
                    enabledBorder: const UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey[800]!,
                      ),
                    ),
                    hintText: 'Password',
                    hintStyle: const TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic,
                    ),
                    suffixIcon: GestureDetector(
                      child: Icon(
                        context.read<LoginBloc>().showPass
                            ? Icons.visibility_off_outlined
                            : Icons.visibility_outlined,
                        color: context.read<LoginBloc>().showPass
                            ? Colors.grey
                            : Colors.grey[700],
                        size: 24,
                      ),
                      onTap: () =>
                          context.read<LoginBloc>().add(ShowPassword()),
                    ),
                  ),
                  obscureText: context.read<LoginBloc>().showPass,
                );
              },
            ),
            BlocBuilder<LoginBloc, LoginState>(
              builder: (context, state) {
                return Container(
                  alignment: Alignment.centerRight,
                  margin: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.purple[800],
                      fixedSize: const Size(150, 40),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(100),
                      ),
                    ),
                    onPressed: () {
                      (context.read<LoginBloc>().username.text.isEmpty ||
                              context.read<LoginBloc>().password.text.isEmpty)
                          ? _alertDialog(context)
                          : context.read<LoginBloc>().add(SubmitLogin());
                    },
                    child: (state is LoginLoading)
                        ? const SizedBox(
                            height: 20,
                            width: 20,
                            child: CircularProgressIndicator(
                              color: Colors.white,
                            ),
                          )
                        : const Text("LOGIN"),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Future _alertDialog(BuildContext context) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            "Login Failed",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.red,
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          content: const Text(
            'User ID dan atau Password anda belum diisi.',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
          actions: [
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.purple[900],
                fixedSize: const Size(100, 40),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
              child: const Text("OK"),
            ),
          ],
          actionsAlignment: MainAxisAlignment.center,
        );
      },
    );
  }
}
