part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();

  @override
  List<Object> get props => [];
}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}

class LoginSucces extends LoginState {}

class LoginFailed extends LoginState {
  final String message;

  const LoginFailed(this.message);
  @override
  List<Object> get props => [message];
}

class LoginCheck extends LoginState {
  final String message;

  const LoginCheck(this.message);
  @override
  List<Object> get props => [message];
}

class ShowPassState extends LoginState {
  final bool status;

  const ShowPassState(this.status);

  @override
  List<Object> get props => [status];
}
