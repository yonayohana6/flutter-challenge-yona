import 'package:auth_products/app/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial()) {
    on<SubmitLogin>((event, emit) async {
      final isValid = formKey.currentState!.validate();
      if (isValid && username.text.isNotEmpty && password.text.isNotEmpty) {
        emit(LoginLoading());
        await repo
            .login(username.text, password.text)
            .then((value) => statusCode = value);
        // print(statusCode);
        if (statusCode == 200) {
          emit(LoginSucces());
        } else {
          emit(
            const LoginFailed('User ID atau Password anda salah'),
          );
        }
      }
    });

    on<ShowPassword>((event, emit) {
      emit(ShowPassState(showPass = !showPass));
    });
  }

  // variable api repository
  final repo = ApiServices();
  final url = Constan.baseUrl;
  int statusCode = 0;

  bool showPass = true;

  final formKey = GlobalKey<FormState>();
  final username = TextEditingController();
  final password = TextEditingController();
}
