part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeSuccess extends HomeState {}

class HomeFailed extends HomeState {
  final String msg;

  const HomeFailed(this.msg);
  @override
  List<Object> get props => [msg];
}
