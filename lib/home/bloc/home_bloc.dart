import 'package:auth_products/app/app.dart';
import 'package:auth_products/home/home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<GetProducts>((event, emit) async {
      emit(HomeLoading());
      await repo.getProduct().then((value) {
        listProduct = value;
      }).onError((error, stackTrace) {
        emit(HomeFailed(error.toString()));
      });
      emit(HomeSuccess());
    });
  }

  final repo = ApiServices();
  List<Product> listProduct = [];
  bool loading = true;
}
